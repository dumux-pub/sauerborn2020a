// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Non-isothermal gas injection problem where a gas (e.g. air)
 *        is injected into a fully water saturated medium.
 */
#ifndef DUMUX_BRINE_AIR_PROBLEM_HH
#define DUMUX_BRINE_AIR_PROBLEM_HH

// The DUNE grid used
#if  HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#elif HAVE_UG
#include <dune/grid/uggrid.hh>
#else
#include <dune/grid/yaspgrid.hh>
#endif // HAVE_DUNE_ALUGRID, HAVE_UG

#include <dumux/io/gnuplotinterface.hh>

#include <dumux/discretization/cctpfa.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p2c/model.hh>
#include <dumux/material/components/h2.hh>
#include <dumux/material/components/supercriticalh2o.hh>
//#include <dumux/material/fluidsystems/h2oair.hh>

#include <dumux/material/fluidsystems/h2oh2.hh>
#include "h2tables.hh"


#include "brineairspatialparams.hh"
//////////Entscheidet ob die Simulation Isotherm ist (ISOTHERMAL 1) oder non-isotherm (ISOTHERMAL 0). Muss jedes mal neu gebuildet werden////////////////7
#define ISOTHERMAL 1

namespace Dumux{

#ifndef ENABLECACHING
#define ENABLECACHING 0
#endif

template <class TypeTag>
class BrineAirProblem;

namespace Properties{
// Create new type tags
namespace TTag {
#if !ISOTHERMAL
    struct BrineAirProblem { using InheritsFrom = std::tuple<CCTpfaModel, TwoPTwoCNI>; };
#endif
#if ISOTHERMAL
    struct BrineAirProblem { using InheritsFrom = std::tuple<CCTpfaModel, TwoPTwoC>; };
#endif
} // end namespace TTag

// Set the grid type
template<class TypeTag>
#if HAVE_DUNE_ALUGRID
struct Grid<TypeTag, TTag::BrineAirProblem> { using type = Dune::ALUGrid</*dim=*/3, 3, Dune::cube, Dune::nonconforming>; };
#elif HAVE_UG
struct Grid<TypeTag, TTag::BrineAirProblem> { using type = Dune::UGGrid<3>; };
#else
struct Grid<TypeTag, TTag::BrineAirProblem> { using type = Dune::YaspGrid<3>; };
#endif

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::BrineAirProblem> { using type = BrineAirProblem<TypeTag>; };

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::BrineAirProblem>
{
    //using type = FluidSystems::H2OAir<GetPropType<TypeTag, Properties::Scalar>>;
    using type = FluidSystems::H2OH2<GetPropType<TypeTag, Properties::Scalar>,
                                     H2Tables::H2Tables,
                                     H2OTables::H2OTables >;

};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::BrineAirProblem>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = BrineAirSpatialParams<GridGeometry, Scalar>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::BrineAirProblem> { static constexpr bool value = false; };

// Enable caching or not (reference solutions created without caching)
template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::BrineAirProblem> { static constexpr bool value = ENABLECACHING; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::BrineAirProblem> { static constexpr bool value = ENABLECACHING; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::BrineAirProblem> { static constexpr bool value = ENABLECACHING; };
}


/*!
 * \ingroup TwoPTwoCModel
 * \ingroup ImplicitTestProblems
 * \brief Non-isothermal gas injection problem where a gas (e.g. air)
 *        is injected into a fully water saturated medium. During
 *        buoyancy driven upward migration the gas passes a high
 *        temperature area.
 *
 * The domain is sized 40 m times 40 m in a depth of 1000 m. The rectangular area
 * with the increased temperature (380 K) starts at (20 m, 1 m) and ends at
 * (30 m, 30 m).
 *
 * For the mass conservation equation neumann boundary conditions are used on
 * the top and on the bottom of the domain, while dirichlet conditions
 * apply on the left and the right boundary.
 * For the energy conservation equation dirichlet boundary conditions are applied
 * on all boundaries.
 *
 * Gas is injected at the bottom boundary from 15 m to 25 m at a rate of
 * 0.001 kg/(s m), the remaining neumann boundaries are no-flow
 * boundaries.
 *
 * At the dirichlet boundaries a hydrostatic pressure, a gas saturation of zero and
 * a geothermal temperature gradient of 0.03 K/m are applied.
 *
 * The model is able to use either mole or mass fractions. The property useMoles can be set to either true or false in the
 * problem file. Make sure that the according units are used in the problem setup. The default setting for useMoles is true.
 *
 * This problem uses the \ref TwoPTwoCModel and \ref NIModel model.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box2p2cni</tt> or
 * <tt>./test_cc2p2cni</tt>
 *  */
template <class TypeTag >
class BrineAirProblem : public PorousMediumFlowProblem<TypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;

    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    
    // copy some indices for convenience
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;
    enum 
    {
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        wCompIdx = FluidSystem::H2OIdx,
        nCompIdx = FluidSystem::H2Idx,

        nPhaseIdx = FluidSystem::H2Idx,
        wPhaseIdx = FluidSystem::H2OIdx,


#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,
#endif

        //Phase State
        wPhaseOnly = Indices::firstPhaseOnly,
        bothPhases = Indices::bothPhases,

        //Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
        
        //Equations indices
        contiWEqIdx = Indices::conti0EqIdx + FluidSystem::H2OIdx,
        contiNEqIdx = Indices::conti0EqIdx + FluidSystem::H2Idx,

    };

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    //typedef Dumux::Components::Air <Scalar> Air;
    //typedef Dumux::Components::H2 <Scalar,  H2Tables> H2;
    using H2O = Dumux::Components::SuperCriticalH2O<Scalar, H2OTables::H2OTables>; //!< The components for pure water
    using H2 = Dumux::Components::H2<Scalar, H2Tables::H2Tables>;
    

    //! Property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = ModelTraits::useMoles();

public:
    /*!
     * \brief The constructor.
     *
     */
    BrineAirProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        //////Ganz kleiner Schritt für Errorfixes und Co/////
        eps_ = 1e-6;
        ////Angaben zur Berechnung von Kenngrößen
        massInjected_ = 0.0;
        stepNumberCount_ = 0.0;
        injectionPressure_ = 0.0;
        /////////////Abgleichsvariablen, um zu wissen, dass wir uns an derselben NewtonIteration befinden////////////
        abgleichsZelleChosen_ = false;
        abgleichsZellePos0_ = 0.0;
        abgleichsZellePos1_ = 0.0;
        abgleichsZellePos2_ = 0.0;   
        /////////////Zählen der verwendeten Zellen während Neumannberechnungen////////////
        numberOfUsedCells_ = 0.0;
        maxNumberOfUsedCells_ = 0.0;
        ////////////Aufaddierte Werte innerhalb der Zellen, um Durchschnittswerte und Co zu berechnen///////////
        pressure_in_aquiSum_ = 0.0;
        massInjectedFlowSum_ = 0.0;
        massInjectedSum_ = 0.0;
        temperature_aquiferSum_ = 0.0;
        pressure_wellSum_ = 0.0;
        energy_flowSum_ = 0.0;
        energy_injectedSum_ = 0.0;
        ///////WIRKUNGSGRADBERECHNUNGSVARIABLEN
            /////////////////////Variablen zur Berechnung von Durchschnittstemperatur//////////////////
            summedUpTemperatureInj_ = 0.0;
            meanTemperatureInj_ = 0.0;
            summedUpTemperatureExt_ = 0.0;
            meanTemperatureExt_ = 0.0;
            /////////////////////Variablen zur Berechnung des Durchschnittsdrucks////////////////
            summedUpPressureInj_ = 0.0;
            meanPressureInj_ = 0.0;
            summedUpPressureExt_ = 0.0;
            meanPressureExt_ = 0.0;
            /////////////////////Variablen zur Berechnungs des Durchschnittswirkungsgrades///////////
            cycleNumber_ = 0.0;
            summedUpEfficiencies_ = 0.0;
            stepNumberCountInj_ = 0.0;
            stepNumberCountExt_ = 0.0;
            ////////////////////Variablen zur Wirkungsgradberechnung mit Energie - selten verwendet///////////////
#if !ISOTHERMAL
            energyInject_ = 0.0;
            energyExtract_ = 0.0;
#endif
        ///////////Implementation für den Injektions/Extraktionsprozess
        isExtracting_ = false;
        isFirstChange_ = false;
        
        FluidSystem::init();

        name_ = getParam<std::string>("Problem.Name");

        //stating in the console whether mole or mass fractions are used
        if(useMoles)
        {
            std::cout<<"problem uses mole-fractions"<<std::endl;
        }
        else
        {
            std::cout<<"problem uses mass-fractions"<<std::endl;
        }
        this->spatialParams().plotMaterialLaw();
    }

    void setTime( Scalar time )
    {
        time_ = time;
    }

    void setTimeStepSize( Scalar timeStepSize )
     {
        timeStepSize_ = timeStepSize;
     }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return name_; }

#if ISOTHERMAL
    /*!
     * \brief Returns the temperature within the domain.
     *
     * \param element The element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param scvIdx The local vertex index (SCV index)
     *
     */
    Scalar temperature() const
    {
        return getParam<Scalar>("Problem.IsothermalTemperature"); // -> 40°C aus Inputfile
    }
#endif

    /*!
     * \brief Returns the source term at specific position in the domain.
     *
     * \param values The source values for the primary variables
     * \param globalPos The position
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^3*s) or kg/(m^3*s))
     */
    
    NumEqVector source(const Element &element,
                   const FVElementGeometry& fvGeometry,
                   const ElementVolumeVariables& elemVolVars,
                   const SubControlVolume &scv) const
    {
        NumEqVector sourceValues(0.0);
        return sourceValues;
    }
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;
    ///////////////////3D Problem////////////////////////
    /////Bestimmen, ob Dirichlet oder Neumann-Randbedingung//////////7
        Scalar radialDistance = sqrt(pow(globalPos[0],2.0)+pow(globalPos[1],2.0));

//         if(radialDistance > ((getParam<std::vector<Scalar>>("Grid.Radial0")[1]) - (eps_ + ((getParam<std::vector<Scalar>>("Grid.Radial0")[1])/getParam<Scalar>("Grid.Cells0")))) && globalPos[1] > eps_ && globalPos[2] > eps_ && globalPos[2] < getParam<std::vector<Scalar>>("Grid.Axial2")[1] && ((tan(((M_PI*(getParam<std::vector<Scalar>>("Grid.Angular1")[1]))/180.0))) * globalPos[0]) - eps_ > globalPos[1])
        if(radialDistance > ((getParam<std::vector<Scalar>>("Grid.Radial0")[1])/2) && globalPos[1] > eps_ && globalPos[2] > eps_ && globalPos[2] < getParam<std::vector<Scalar>>("Grid.Axial2")[1] && ((tan(((M_PI*(getParam<std::vector<Scalar>>("Grid.Angular1")[1]))/180.0))) * globalPos[0]) - eps_ > globalPos[1])
            bcTypes.setAllDirichlet();
        else
            bcTypes.setAllNeumann();
        return bcTypes;
    }
    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        return initial_(globalPos);
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann
     *        boundary segment in dependency on the current solution.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param elemVolVars All volume variables for the element
     * \param scvf The sub-control volume face
     *
     * This method is used for cases, when the Neumann condition depends on the
     * solution and requires some quantities that are specific to the fully-implicit method.
     * The \a values store the mass flux of each phase normal to the boundary.
     * Negative values indicate an inflow.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
        ////////////////Neumann-Randbedingung für Energie einbauen/////////////
    {
        NumEqVector neumannValues(0.0);
//         const auto& globalPos = scv.dofPosition();
        const auto& globalPos = scvf.ipGlobal();


        ////////////////Neumann-Randbedingung für Energie einbauen - An den Seiten (links und rechts ist es No-flow, da die Strömung vom Zentrum aus als parallel angenommen wird)////////////
        if(globalPos[1] > eps_ && ((tan(((M_PI*(getParam<std::vector<Scalar>>("Grid.Angular1")[1]))/180.0))) * globalPos[0]) - eps_ > globalPos[1] && sqrt(pow(globalPos[0],2.0)+pow(globalPos[1],2.0)) > getParam<Scalar>("Grid.WellRadius") + eps_)
        {
            ///////////////ZUSAMMENSETZUNG VON SOMERTON CONDUCTIVITY COEFFICIENT///////////
//             static Scalar effectiveThermalConductivity(const Scalar satLiquid,
//             const Scalar lambdaLiquid,
//             const Scalar lambdaGas,
//             const Scalar lambdaSolid,
//             const Scalar porosity,
//             const Scalar rhoSolid = 0.0 /*unused*/
#if !ISOTHERMAL
            Scalar HeatTransferCoefficientNeumann(0.0);
            if(getParam<Scalar>("Component.HeatTransferCoefficientMethod") == 0.0)
            {
            using std::max;
            using std::pow;
            using std::sqrt;
            const Scalar satLiquidPhysical = max<Scalar>(0.0, getParam<Scalar>("Component.LiquidPhaseResidualSaturation"));
            // geometric mean, using ls^(1-p)*l^p = ls*(l/ls)^p
            const Scalar lSat = getParam<Scalar>("Component.SolidThermalConductivity") * pow(getParam<Scalar>("Component.LiquidThermalConductivity") / getParam<Scalar>("Component.SolidThermalConductivity"), this->spatialParams().porosityAtPos(globalPos));
            const Scalar lDry = getParam<Scalar>("Component.SolidThermalConductivity") * pow(getParam<Scalar>("Component.GasThermalConductivity") / getParam<Scalar>("Component.SolidThermalConductivity"), this->spatialParams().porosityAtPos(globalPos));

            HeatTransferCoefficientNeumann = lDry + sqrt(satLiquidPhysical) * (lSat - lDry);
            ///////////////DebugPrint/////////////
//             std::cout << "HeatTransferCoefficientNeumann: " << HeatTransferCoefficientNeumann << std::endl;
            }
            if(getParam<Scalar>("Component.HeatTransferCoefficientMethod") == 1.0)
            {
            HeatTransferCoefficientNeumann = getParam<Scalar>("Component.HeatTransferCoefficientForced");
            }
            neumannValues[energyEqIdx] = (HeatTransferCoefficientNeumann/scvf.geometry().volume()) * (elemVolVars[scvf.insideScvIdx()].temperature() - (getParam<Scalar>("Problem.TemperaturGanzOben") + ((getParam<std::vector<Scalar>>("Grid.Axial2")[1]) + (getParam<std::vector<Scalar>>("Grid.Axial2")[1]) - getParam<Scalar>("Grid.DomeVerschiebungOutside") - globalPos[2]) * getParam<Scalar>("Problem.TemperaturZunahmeUnten")));
            
#endif
#if ISOTHERMAL
            NumEqVector neumannValues(0.0);
#endif
        ////////////////mit fester Temperatur/////////////////////
//             NeumannValues[energyEqIdx] = getParam<Scalar>("Problem.HeatTransferCoefficient") * (elemVolVars[scvf.insideScvIdx()].temperature() - getParam<Scalar>("TemperaturImAquiferIsotherm"));
        }

        
        /////////Implementation des Injektions/Extraktionsprozess//////////////
        ////Werte für Peaceman-Well-Gleichung/////
        Scalar cellHeight = getParam<Scalar>("Problem.cellHeight");// Aus Inputfile (0.41)
        Scalar effectiveWellRadius = 0.2*cellHeight;
        Scalar WellDiameter = getParam<Scalar>("Problem.WellDiameter"); // Aus Inputfile (0.05335);
        Scalar permeability = this->spatialParams().permeabilityAtPos(globalPos);
        Scalar mobilityGas = std::max(0.1, elemVolVars[scvf.insideScvIdx()].mobility(nPhaseIdx));
//         Scalar mobilityWater = std::max(0.1, elemVolVars[scvf.insideScvIdx()].mobility(wPhaseIdx));
        Scalar WellIndex =(2.0*M_PI*cellHeight)/log(effectiveWellRadius/WellDiameter);
        Scalar densityNw = std::max(elemVolVars[scvf.insideScvIdx()].density(nPhaseIdx), 60.0);
//         Scalar densityW = std::max(elemVolVars[scvf.insideScvIdx()].density(wPhaseIdx), 800.0);
        /////////////////WellIndex gegebenenfalls zu 1 oder zu einem fixen Faktor machen, um ihn verschwinden zu lassen, falls er nicht benötigt wird - Im Input definierbar//////////////////
        if(getParam<Scalar>("Problem.WellIndexExists") == 0.0)
        {
            WellIndex = 1.0;
        }
        if(getParam<Scalar>("Problem.WellIndexExists") == 1.0)
        {
            WellIndex = getParam<Scalar>("Problem.WellIndexReplace");
        }
        
        //////////3 Checks für Position: Höhe korrekt; Radius an der korrekten Postion; Winkel korrekt - WINKEL WIRD VERNACHLÄSSIGT, da 360° angenommen wird///////////////////////
        /////////////Achtung Je nach Grading kann es errors geben///////////////
        ////////Höhencheck///////////
        if(globalPos[2] > getParam<Scalar>("Problem.InjectionPointZAxis") - (getParam<Scalar>("Problem.InjectionAreaHeight")/2.0) + eps_ && globalPos[2] < getParam<Scalar>("Problem.InjectionPointZAxis") +(getParam<Scalar>("Problem.InjectionAreaHeight")/2.0) - eps_)
        {
            //////////////Radius Check///////////
            if(sqrt(pow(globalPos[0],2.0)+pow(globalPos[1],2.0)) < getParam<Scalar>("Grid.WellRadius") + eps_)
            {
                ///////////////Winkelcheck////////
//                 if(tan((M_PI/180.0)*(getParam<std::vector<Scalar>>("Grid.Angular1")[1])+((getParam<std::vector<Scalar>>("Grid.Angular1")[1])/(getParam<Scalar>("Grid.Cells1")*2.0)))) > (globalPos[1]/globalPos[0]) && tan((M_PI/180.0)*((getParam<std::vector<Scalar>>("Grid.Angular1")[1])-((getParam<std::vector<Scalar>>("Grid.Angular1")[1])/(getParam<Scalar>("Grid.Cells1")*2.0)))) < (globalPos[1]/globalPos[0]))
//                 {
                /////Proably ein Bugfix, der die Loop nicht am Nullpunkt procct - Erster Schritt ist zum der Anzahl der Zellen notwendig)//////////////
//                 if(time_ > 1.0)
//                 {
                    //////////////////////Wenn die Limit-Masse (Im Moment 2000) injiziert wurde wird es Zeit mit dem Extrahieren zu beginnen/////////////////////////
                    if(!isExtracting_ && massInjected_ >= getParam<Scalar>("Problem.InjectionMassLimit"))
                    {    
                        isExtracting_ = true;
                        ///////////////////Step rausschreiben ist notwendig, um das multiple Durchlaufen der Loop zu ignorieren (Jede Newton-Iteration durchläuft die Schleife ein neues Mal durch, beim zweitem Mal wird die Loop nicht nochmal durchlaufen)
                        stepKontrolle_ = stepNumberCount_;
                        isFirstChange_ = true;
                    }
                    ///////////////////////Wenn im Moment extrahiert wird und das Minimum (0) erreicht wird, wird wieder injiziert////////////////////
                    if(isExtracting_ && massInjected_ <= (0 - eps_))
                    {   
                        isExtracting_ = false;
                        /////////////Step rausschreiben ist notwendig, um das multiple Durchlaufen der Loop zu ignorieren (Jede Newton-Iteration durchläuft die Schleife ein neues Mal durch, beim zweitem Mal wird die Loop nicht nochmal durchlaufen)
                        stepKontrolle_ = stepNumberCount_;
                        isFirstChange_ = true;
                    }
                    //////////////////////Injektionsberechnung/////////////////////////////////
                    if(!isExtracting_ && getParam<Scalar>("Problem.InjectionMethod") == 0.0)
                    {
                        /////////////////////////////Druck zur Berechnung//////////////////////////////////////
                        injectionPressure_ = getParam<Scalar>("Problem.InjectionPressure");
                        ////////////////////////////////////Injektionsmengenberechnung///////////////////////////////
                        neumannValues[contiNEqIdx] = (-1.0) * mobilityGas*WellIndex*densityNw*permeability*(injectionPressure_-elemVolVars[scvf.insideScvIdx()].pressure(nPhaseIdx))*(1.0/scvf.geometry().volume()); //[kg/m²s]
                    }
                    if(!isExtracting_ && getParam<Scalar>("Problem.InjectionMethod") == 1.0)
                    {
                        //////////////////////EXTRAKTIONSMENGE/////////////////////////
                        neumannValues[contiNEqIdx] = (-1.0) * getParam<Scalar>("Problem.InjectionRate") / (scvf.geometry().volume()*maxNumberOfUsedCells_);
                        ///////////////////Wird nur zum Ausschreiben berechnet/////////////////////////////
//                         injectionPressure_=(neumannValues[contiNEqIdx]*(scvf.geometry().volume())+elemVolVars[scvf.insideScvIdx()].massFraction(nPhaseIdx,nCompIdx)*mobilityGas*WellIndex*densityNw*permeability*elemVolVars[scvf.insideScvIdx()].pressure(nPhaseIdx)+elemVolVars[scvf.insideScvIdx()].massFraction(wPhaseIdx,nCompIdx)*mobilityWater*WellIndex*densityW*permeability*elemVolVars[scvf.insideScvIdx()].pressure(wPhaseIdx))*(1.0/(elemVolVars[scvf.insideScvIdx()].massFraction(nPhaseIdx,nCompIdx)*mobilityGas*WellIndex*densityNw*permeability+elemVolVars[scvf.insideScvIdx()].massFraction(wPhaseIdx,nCompIdx)*mobilityWater*WellIndex*densityW*permeability));
                        injectionPressure_ = (((-1.0)*neumannValues[contiNEqIdx]*scvf.geometry().volume())/(mobilityGas*WellIndex*densityNw*permeability))+elemVolVars[scvf.insideScvIdx()].pressure(nPhaseIdx);
                    }
                    /////////////////////////////Extraktionsberechnung/////////////////////////////
                    if(isExtracting_)
                    {
                        //////////////////////EXTRAKTIONSMENGE/////////////////////////
                        neumannValues[contiNEqIdx] = getParam<Scalar>("Problem.ExtractionRate") / (scvf.geometry().volume()*maxNumberOfUsedCells_);
                        ///////////////////Wird nur zum Ausschreiben berechnet/////////////////////////////
//                         injectionPressure_=(neumannValues[contiNEqIdx]*(scvf.geometry().volume())+elemVolVars[scvf.insideScvIdx()].massFraction(nPhaseIdx,nCompIdx)*mobilityGas*WellIndex*densityNw*permeability*elemVolVars[scvf.insideScvIdx()].pressure(nPhaseIdx)+elemVolVars[scvf.insideScvIdx()].massFraction(wPhaseIdx,nCompIdx)*mobilityWater*WellIndex*densityW*permeability*elemVolVars[scvf.insideScvIdx()].pressure(wPhaseIdx))*(1.0/(elemVolVars[scvf.insideScvIdx()].massFraction(nPhaseIdx,nCompIdx)*mobilityGas*WellIndex*densityNw*permeability+elemVolVars[scvf.insideScvIdx()].massFraction(wPhaseIdx,nCompIdx)*mobilityWater*WellIndex*densityW*permeability));
                        injectionPressure_ = (((-1.0)*neumannValues[contiNEqIdx]*scvf.geometry().volume())/(mobilityGas*WellIndex*densityNw*permeability))+elemVolVars[scvf.insideScvIdx()].pressure(nPhaseIdx);
                    }  
///////////////////////Mögliches Überschreiben der Flussgeschwindigkeit und damit der injzierten Masse values[contiNEqIdx] zu null -> Um Diffusion zu erlauben//////////////////
                            ///////////// Muss weiterhin im selben Zeitraum sein, da nur während vollständiger Injektion bzw. Extraktion gewartet werden kann 
                            /////////Wenn Extraktion und Injektion umpolarisiert, dann warten (Fluss = 0), ansonsten isFirstChange wieder false machen und weitermachen///////////
                    if(stepKontrolle_ > (stepNumberCount_ - getParam<Scalar>("Problem.DowntimeStepsForDiffusion")) && isFirstChange_ == true)
                    {
                        ///////////////Injektion = 0 wenn gewartet werden muss////////////////////////////
                        neumannValues[contiNEqIdx] = 0.0;
//                         injectionPressure_=(neumannValues[contiNEqIdx]*(scvf.geometry().volume())+elemVolVars[scvf.insideScvIdx()].massFraction(nPhaseIdx,nCompIdx)*mobilityGas*WellIndex*densityNw*permeability*elemVolVars[scvf.insideScvIdx()].pressure(nPhaseIdx)+elemVolVars[scvf.insideScvIdx()].massFraction(wPhaseIdx,nCompIdx)*mobilityWater*WellIndex*densityW*permeability*elemVolVars[scvf.insideScvIdx()].pressure(wPhaseIdx))*(1.0/(elemVolVars[scvf.insideScvIdx()].massFraction(nPhaseIdx,nCompIdx)*mobilityGas*WellIndex*densityNw*permeability+elemVolVars[scvf.insideScvIdx()].massFraction(wPhaseIdx,nCompIdx)*mobilityWater*WellIndex*densityW*permeability));
                        injectionPressure_ = (((-1.0)*neumannValues[contiNEqIdx]*scvf.geometry().volume())/(mobilityGas*WellIndex*densityNw*permeability))+elemVolVars[scvf.insideScvIdx()].pressure(nPhaseIdx);
                        
                        ////////////BugfixPrint//////
//                         std::cout << neumannValues[contiNEqIdx] << "; StepnumberCount: " << stepNumberCount_ << "; StepKontrolle: " << stepKontrolle_ << std::endl;
                    }
                    if(stepKontrolle_ <= (stepNumberCount_ - getParam<Scalar>("Problem.DowntimeStepsForDiffusion")) && isFirstChange_ == true)
                    {
                        isFirstChange_ = false;
                    }
                    /////////Wenn die Diffusion notwendig, dann warten (Fluss = 0), ansonsten isFirstChange wieder false machen und weitermachen///////////
                    //////////////Herauslesen der Sättigung und Überprüfung ob Sättigung zu hoch, und falls, dann warten///////////////////
//                     if(isFirstChange_ == true)   
//                     {
//                         if(/*HIER KONTROLLIEREN, OB DIE SÄTTIGUNG ZU HOCH IST*/)
//                         {
//                             neumannValues[contiNEqIdx] = 0.0;
//                             injectionPressure_=(neumannValues[contiNEqIdx]*(scvf.geometry().volume())+elemVolVars[scvf.insideScvIdx()].massFraction(nPhaseIdx,nCompIdx)*mobilityGas*WellIndex*densityNw*permeability*elemVolVars[scvf.insideScvIdx()].pressure(nPhaseIdx)+elemVolVars[scvf.insideScvIdx()].massFraction(wPhaseIdx,nCompIdx)*mobilityWater*WellIndex*densityW*permeability*elemVolVars[scvf.insideScvIdx()].pressure(wPhaseIdx))*(1.0/(elemVolVars[scvf.insideScvIdx()].massFraction(nPhaseIdx,nCompIdx)*mobilityGas*WellIndex*densityNw*permeability+elemVolVars[scvf.insideScvIdx()].massFraction(wPhaseIdx,nCompIdx)*mobilityWater*WellIndex*densityW*permeability));
//                         }
//                         else
//                         {
//                             isFirstChange_ = false;
//                         }
//                     }
//                 }////GEHÖRT ZUM BUGFIX MIT TIME > 1.0
        ///////////////////////////////Energieberechnungen/////////////////////////////////////////
#if !ISOTHERMAL
                    Scalar injectionTemperature = getParam<Scalar>("Problem.InjectionTemperatureInput");;//wie bei Huntorf: -51,15°C
                    if(!isExtracting_)
                    {
                        neumannValues[energyEqIdx] = neumannValues[contiNEqIdx] * H2::gasEnthalpy(injectionTemperature, injectionPressure_);
                    }
                    else
                    {
                        neumannValues[energyEqIdx] = neumannValues[contiNEqIdx] * H2::gasEnthalpy(elemVolVars[scvf.insideScvIdx()].temperature(), elemVolVars[scvf.insideScvIdx()].pressure(nPhaseIdx)) + neumannValues[contiWEqIdx] * H2O::liquidEnthalpy(elemVolVars[scvf.insideScvIdx()].temperature(), elemVolVars[scvf.insideScvIdx()].pressure(wPhaseIdx));            
                    }
#endif
                    ///////////////Berechnungen fürs PoststepVerhalten - Output Berechnungen////////////
                    ///////////Überprüfen ob es eine andere NewtonIteration ist/////////////////////
                    ///////Wenn noch nicht vorhanden, Abgleichszelle wählen///////////////////
                    if(abgleichsZelleChosen_ == false)
                    {
                        abgleichsZelleChosen_ = true;
                        abgleichsZellePos0_ = globalPos[0];
                        abgleichsZellePos1_ = globalPos[1];
                        abgleichsZellePos2_ = globalPos[2];
                    }
                    //////////////Überprüfen ob es dieselbe Zelle ist, falls ja alles resetten//////////////////
//                     if(abgleichsZellePos0_ + eps_ > globalPos[0] && abgleichsZellePos0_ - eps_ < globalPos[0])
//                     {
//                         if(abgleichsZellePos1_ + eps_ > globalPos[1] && abgleichsZellePos1_ - eps_ < globalPos[1])
//                         {
//                             if(abgleichsZellePos2_ + eps_ > globalPos[2] && abgleichsZellePos0_ - eps_ < globalPos[2])
//                             {
                    if(abgleichsZellePos0_ == globalPos[0])
                    {
                        if(abgleichsZellePos1_ == globalPos[1])
                        {
                            if(abgleichsZellePos2_ == globalPos[2])
                            {
                                //////////ALLES AUF 0.0 SETZEN////////////
                                if(maxNumberOfUsedCells_ < numberOfUsedCells_)
                                {
                                    maxNumberOfUsedCells_ = numberOfUsedCells_;
                                }
                                numberOfUsedCells_ = 0.0;
                                pressure_in_aquiSum_ = 0.0;
                                massInjectedFlowSum_ = 0.0;
                                massInjectedSum_ = 0.0;
                                temperature_aquiferSum_ = 0.0;
                                pressure_wellSum_ = 0.0;
#if !ISOTHERMAL
                                energy_flowSum_ = 0.0;
                                energy_injectedSum_ = 0.0;
#endif
                            }
                        }
                    }
                    ///////////Werte auffüllen//////////
                    numberOfUsedCells_ += 1.0;
                    pressure_in_aquiSum_ += elemVolVars[scvf.insideScvIdx()].pressure(nPhaseIdx);


#if ISOTHERMAL      
                    if(!isExtracting_ && getParam<Scalar>("Problem.InjectionMethod") == 0.0)
                    {
                        massInjectedFlowSum_ += (-1.0) * ((45.0)/(34.0)) * neumannValues[contiNEqIdx] * (scvf.geometry().volume());
                        massInjectedSum_ += (-1.0) * ((45.0)/(34.0)) * neumannValues[contiNEqIdx] * (scvf.geometry().volume()) * timeStepSize_;
                    }
                    else
                    {
                        massInjectedFlowSum_ += (-1.0) * neumannValues[contiNEqIdx] * (scvf.geometry().volume());
                        massInjectedSum_ += (-1.0) * neumannValues[contiNEqIdx] * (scvf.geometry().volume()) * timeStepSize_;
                    }
#endif
#if !ISOTHERMAL
                    massInjectedFlowSum_ += (-1.0) * neumannValues[contiNEqIdx] * (scvf.geometry().volume());
                    massInjectedSum_ += (-1.0) * neumannValues[contiNEqIdx] * (scvf.geometry().volume()) * timeStepSize_;
#endif

                    temperature_aquiferSum_ += elemVolVars[scvf.insideScvIdx()].temperature();
                    pressure_wellSum_ += injectionPressure_;
#if !ISOTHERMAL
                    energy_flowSum_ += /*(-1.0) **/ neumannValues[energyEqIdx] * (scvf.geometry().volume());
                    energy_injectedSum_ += /*(-1.0) **/ neumannValues[energyEqIdx] * (scvf.geometry().volume()) * timeStepSize_;
#endif
//                 }////////////Gehört zum Time > 1 check
//                 }//////Gehört zum Winkelcheck und ist daher obsolet
            }
        }
        return neumannValues;     
    }

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param values The initial values for the primary variables
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        return initial_(globalPos);
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param globalPos The global position
     */
    int initialPhasePresenceAtPos(const GlobalPosition &globalPos) const
    {
        return bothPhases;
    }

/////////////////////AUSSCHREIBEN VON ALLEN DATEN IN GNUGNUGNU///////////////////////////////
  void postTimeStep(const SolutionVector& curSol,
                      const GridVariables& gridVariables,
                      const Scalar time,
                      const Scalar timeStepSize)
    {
////////////////////Calculate mass of H2 desolved in the water phase in the hole domain////////////
        //int start = 0;
        MassTotal = 0;
        Scalar Molarmass_H2 = 0.00201588; // Molarmass H2 kg/mol
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            MassSCV = 0;
            const auto elemSol = elementSolution(element, curSol, this->gridGeometry());
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto dofIdxGlobal = scv.dofIndex();
                
                //Get Params
                Permeability = volVars.permeability();
                Porosity     = volVars.porosity();
                Saturation   = volVars.saturation(0);
                Molefrac     = volVars.moleFraction(0, 1);
                Molardensity = volVars.molarDensity(0);
                Volume       = scv.geometry().volume();
                
                
                // Calculate mass in scv
                Mass         = Porosity * Saturation * Volume* Molefrac *  Molardensity * Molarmass_H2 ;
                //Calculate mass in element
                MassSCV += Mass ;
            }
            //Calculate mass in the hole domain
            MassTotal += MassSCV;
        }
        //Write Output in text
        using namespace std;
        //if(start == 0){
        //    fstream f;
        //    f.open("test_masse.dat", ios::out);
        //    f << "time";
        //    f << "\t";
        //    f << "mass";
        //    f << "\n" << endl;
        //    f.close();
        //    start = start + 1;
        //}

        fstream f;
        f.open("test_masse.dat", ios::app);
        f << time_;
        f << "\t";
        f << MassTotal;
        f << "\n";
///////////////////////////////////////////////////////////////////////////
        Scalar inflow(0.0);
        Scalar pressure_in(0.0);
        Scalar temperature_in(0.0);
#if !ISOTHERMAL
        Scalar energy_inflow(0.0);
#endif
        Scalar pressure_well(0.0);
        static std::vector<double> efficiencyEachCycle;
        
        /////////////DebugPrint/////////////////////
//         std::cout << "number of used cells: " << numberOfUsedCells_ << std::endl;
        ////////////////Berechnungen von allen Outputwerten (Durchschnitt und Co)////////////////
        pressure_in = (pressure_in_aquiSum_/numberOfUsedCells_);
        inflow = massInjectedFlowSum_;
        massInjected_ += (massInjectedSum_/1000);
        temperature_in = (temperature_aquiferSum_/numberOfUsedCells_);
        pressure_well = (pressure_wellSum_/numberOfUsedCells_);
#if !ISOTHERMAL
        energy_inflow = energy_flowSum_;
        energyInjected_ += energy_injectedSum_;
#endif
        ///////Für nächsten Schritt alles resetten////////////
        abgleichsZelleChosen_ = false;
        
        ///////////////////Wirkungsgradberechnung (Beta-Test-Crap)////////////////////
        ////////////Es existieren zwei Möglichkeiten: Einmal mit Exergie, einmal mit Energiefluss//////////////////
        ////////////Der Shotcall erfolgt in der Inputdatei//////////////
        if(getParam<Scalar>("Problem.EfficiencyCalculationChoice") == 0.0)
        {
            //////////Variablen zur Berechnung von Injektionskram//////////////////
            if(!isExtracting_ && isFirstChange_ == false)
            {
                ///////////Es gibt zwei Methoden zur Berechnung von Injektionsdruck und Temperatur:Entweder aus InputFile rausgelesen oder berechnet von Dumux//////////
                ////////////////////Aus dem Inputfile/////////////////
                if(getParam<Scalar>("Problem.TemperatureAndPressureInjection") != 1.0)
                {
                    meanTemperatureInj_ = getParam<Scalar>("Problem.InjectionTemperatureInput");
                    meanPressureInj_ = getParam<Scalar>("Problem.InjectionPressure");
                }
                /////////////////Alternative Berechnung von Injektionsangaben. Für diese Kalkulation werden die Angaben von Dumux verwendet////////////////////
                if(getParam<Scalar>("Problem.TemperatureAndPressureInjection") == 1.0)
                {
                    summedUpTemperatureInj_ += temperature_in * massInjectedFlowSum_ * timeStepSize_;
                    stepNumberCountInj_ += massInjectedFlowSum_ * timeStepSize_;
                    meanTemperatureInj_ = (summedUpTemperatureInj_ / stepNumberCountInj_);
                    summedUpPressureInj_ += pressure_in;
                    meanPressureInj_ = (summedUpPressureInj_ / stepNumberCountInj_);
                }
            }
            //////////Variablen zur Berechnung des Extraktionskram////////////////
            if(isExtracting_ && isFirstChange_ == false)
            {
                summedUpTemperatureExt_ += temperature_in;
                stepNumberCountExt_ += 1.0;
                meanTemperatureExt_ = (summedUpTemperatureExt_ / stepNumberCountExt_);
                summedUpPressureExt_ += pressure_in;
                meanPressureExt_ = (summedUpPressureExt_ / stepNumberCountExt_);
            }
            ///////////DebugPrint/////////////
//             std::cout << "SpecificHeatCap: " << getParam<Scalar>("Problem.SpecificHeatCap") << std::endl;
// //             std::cout << "Injektionstemperatur Summe: " << summedUpTemperatureInj_ << std::endl;
//             std::cout << "Injektionstemperaturdurchschnitt: " << meanTemperatureInj_ << std::endl;
// //             std::cout << "Injektionsdruck Summe: " << summedUpPressureInj_ << std::endl;
//             std::cout << "Injektionsdruckdurchschnitt: " << meanPressureInj_ << std::endl;
//             std::cout << "Extraktionstemperatur Summe: " << summedUpTemperatureExt_ << std::endl;
//             std::cout << "Extraktionstemperaturdurchschnitt: " << meanTemperatureExt_ << std::endl;
//             std::cout << "Extraktionsdruck Summe: " << summedUpPressureExt_ << std::endl;
//             std::cout << "Extraktionsdruckdurchschnitt: " << meanPressureExt_ << std::endl;
            
            ////////////Rausschreiben der Zyklenanzahl und Ermittlung des Wirkungsgrades///////////
            if(!isExtracting_ && isFirstChange_ == true && summedUpTemperatureExt_ != 0.0)
            {
                ///Zyklenzahl + 1///
                static std::vector<double> cycleNumber;
                cycleNumber_ += 1.0;

                ///////////////Berechnung des Wirkungsgrades eines Zykluses mit allen Sachen////////////////
                exergyLoss_ = ((getParam<Scalar>("Problem.SpecificHeatCap") * (meanTemperatureInj_ - meanTemperatureExt_)) - (getParam<Scalar>("Problem.Umgebungstemperatur") * ((getParam<Scalar>("Problem.SpecificHeatCap") * log(meanTemperatureInj_/meanTemperatureExt_))-(getParam<Scalar>("Problem.GaskonstanteLuft") * log(meanPressureInj_/meanPressureExt_)))));
                exergyInput_ = ((getParam<Scalar>("Problem.SpecificHeatCap") * (meanTemperatureInj_ - getParam<Scalar>("Problem.Umgebungstemperatur"))) - (getParam<Scalar>("Problem.Umgebungstemperatur") * ((getParam<Scalar>("Problem.SpecificHeatCap") * log(meanTemperatureInj_/getParam<Scalar>("Problem.Umgebungstemperatur")))-(getParam<Scalar>("Problem.GaskonstanteLuft") * log(meanPressureInj_/getParam<Scalar>("Problem.Umgebungsdruck")))))); 
                
                ///////DebugPrint/////////
//                 std::cout << "Exergieverlust: " << exergyLoss_ << std::endl;
//                 std::cout << "ExergieInput: " << exergyInput_ << std::endl;
                efficiencyViaExergy_ = (1 - (exergyLoss_/exergyInput_));
                summedUpEfficiencies_ += efficiencyViaExergy_;
                cycleNumber.push_back(cycleNumber_);
                efficiencyEachCycle.push_back(efficiencyViaExergy_);
                std::cout << "Cycle Number: " << cycleNumber_ << std::endl;
                std::cout << "Wirkungsgrad/Efficiency (via Exergy): " << efficiencyViaExergy_ << std::endl;
                std::cout << "Average Wirkungsgrad/Efficiency (via Exergy): " << (summedUpEfficiencies_/cycleNumber_) << std::endl;
                gnuplot_.addDataSetToPlot(cycleNumber, efficiencyEachCycle, "Efficiency each cycle", "axes x1y1 w l");
                meanTemperatureInj_ = 0.0;
                stepNumberCountInj_ = 0.0;
                summedUpTemperatureInj_ = 0.0;
                summedUpPressureInj_ = 0.0;
                meanPressureInj_ = 0.0;
                meanTemperatureExt_ = 0.0;
                stepNumberCountExt_ = 0.0;
                summedUpTemperatureExt_ = 0.0;
                summedUpPressureExt_ = 0.0;
                meanPressureExt_ = 0.0;
            }
        }  
        
#if !ISOTHERMAL
        /////////////////Berechnung via Energiefluss///////////////
        if(getParam<Scalar>("Problem.EfficiencyCalculationChoice") == 1.0)
        {
            ////////////Aufsummieren der injizierten Energie////////////
            if(!isExtracting_ && isFirstChange_ == false)
            {
                energyInject_ += energy_injectedSum_;
                //////////Print als Bugfix//////////
//                 std::cout << "EnergyInjected Sum: " << energyInject_ << std::endl;
            }
            //////////////Aufsummieren der extrahierten Energie///////////
            if(isExtracting_ && isFirstChange_ == false)
            {
                energyExtract_ += energy_injectedSum_;
                //////////Print als Bugfix//////////
//                 std::cout << "EnergyExtracted Sum: " << energyExtract_ << std::endl; 
            }
            /////////////Berechnung des Wirkungsgrades/////////////
            efficiencyViaEnergy_ = (energyExtract_/energyInject_);
            std::cout << "Wirkungsgrad/Efficiency (via Energy): " << efficiencyViaEnergy_ << std::endl;
        }
#endif
        
        stepNumberCount_ += 1.0;
        std::cout << "inflow: " << inflow << std::endl;
        std::cout << "Step Number: " << stepNumberCount_ << std::endl;
        std::cout << "mass injected: " << massInjected_ << std::endl;
        std::cout << "pressure in aquifer: " << pressure_in << std::endl;
        std::cout << "pressure in well: " << pressure_well << std::endl;
#if !ISOTHERMAL
        std::cout << "energy-inflow: " << (-1.0) * energy_inflow << std::endl;
#endif
        std::cout << "temperature in aquifer: " << temperature_in << std::endl;

/////////////////////////////////EINTRAGEN DER BERECHNETEN WERTE IN GNUGNU//////////////////////////////////
// Gnuplot (In Gnuplot eintragen)
        static double yMax = 0.1;
        static double yMin = -0.1;
        static std::vector<double> x;
        static std::vector<double> y;
        static std::vector<double> y2;
        static std::vector<double> y3;
        static std::vector<double> y4;
        static std::vector<double> y5;
        static std::vector<double> y6;
        static std::vector<double> y7;

////////////////////////VERSCHIEDENE GESAMTBERECHNUNGEN/////////////////////////////////////
        Scalar inflow_plot = inflow;
        Scalar time_plot = time_/3600.0;
        Scalar pressure_in_plot = pressure_in*1e-5;
        Scalar mass_injected_plot = massInjected_;
        Scalar temperature_in_plot = temperature_in - 273.15;
#if !ISOTHERMAL
        Scalar energy_plot = (-1.0) * energy_inflow / 1e4;
        Scalar energy_injected_plot = (-1.0) * energyInjected_ / 1e8;
#endif
        Scalar pressure_well_plot = pressure_well *1e-5;
//         Scalar water_plot = mass_water_extracted * 1000.0;

////////////////////////////////////AUSSCHREIBEN ALLER WERTE//////////////////////////////////77
        x.push_back(time_plot);
//         y.push_back(inflow_plot);
        y.push_back(pressure_in_plot);
        y2.push_back(inflow_plot);
        y3.push_back(mass_injected_plot);
        y4.push_back(temperature_in_plot);
        y5.push_back(pressure_well_plot);
#if !ISOTHERMAL
        y6.push_back(energy_plot);
        y7.push_back(energy_injected_plot);
#endif

       ////////////////////////////ANDERE PUSHBACK WERTE - BONUS EXTRAAAAA///////////////////////
        yMax = std::max({yMax, pressure_in_plot});
        yMin = std::min({yMin, pressure_in_plot});
      /////////////////////Falls inflow_plot statt pressure_in_plot genommen wurde//////////////////////////////////
//         yMax = std::max({yMax, inflow_plot});
      //////////////////////////////////PROBLEMMMMMMMMMMM/////////////////////////////////////////

#if !ISOTHERMAL
        if(time_ > 1.0)
        {
            if(getParam<Scalar>("Problem.EnergyFlowDiagram") == 1.0 && getParam<Scalar>("Problem.EnergyFlowDiagramSum") == 1.0)
            {
                yMax = std::max({yMax, inflow_plot, massInjected_, energy_injected_plot});
                yMin = std::min({yMin, inflow_plot, energy_plot, massInjected_, energy_injected_plot});
            }
            if(getParam<Scalar>("Problem.EnergyFlowDiagram") != 1.0 && getParam<Scalar>("Problem.EnergyFlowDiagramSum") == 1.0)
            {
                yMax = std::max({yMax, inflow_plot, massInjected_, energy_injected_plot});
                yMin = std::min({yMin, inflow_plot, massInjected_, energy_injected_plot});
            }
            if(getParam<Scalar>("Problem.EnergyFlowDiagram") == 1.0 && getParam<Scalar>("Problem.EnergyFlowDiagramSum") != 1.0)
            {
                yMax = std::max({yMax, inflow_plot, massInjected_});
                yMin = std::min({yMin, inflow_plot, energy_plot, massInjected_});
            }
        }
#endif
        
#if ISOTHERMAL
        if(time_ > 1.0)
        {
            yMax = std::max({yMax, inflow_plot, massInjected_});
            yMin = std::min({yMin, inflow_plot, massInjected_});
        }
#endif
        gnuplot_.resetPlot();
        gnuplot_.setXRange(0.0, x.back()+2);
        gnuplot_.setYRange(yMin-2, yMax+2);
        if(getParam<Scalar>("Problem.LanguageGnuplot") == 0.0)
        {
            gnuplot_.setXlabel("Zeit [h]");
      ////////////////////////ALTERNATTIIIVEEEE//////////////////////////////////
            gnuplot_.setYlabel("verschiedene Einheiten");
            gnuplot_.addDataSetToPlot(x, y, "Druck im Aquifer [bar]", "axes x1y1 w l");
            gnuplot_.addDataSetToPlot(x, y2, "Luftmassenstrom [kg per s]", "axes x1y1 w l");
            gnuplot_.addDataSetToPlot(x, y3, "Injizierte Luftmasse [t]", "axes x1y1 w l");
            gnuplot_.addDataSetToPlot(x, y4, "Temperatur im Aquifer [°C]", "axes x1y1 w l");
            gnuplot_.addDataSetToPlot(x, y5, "Druck im Brunnen [bar]", "axes x1y1 w l");
#if !ISOTHERMAL
            if(getParam<Scalar>("Problem.EnergyFlowDiagram") == 1.0)
            {
                gnuplot_.addDataSetToPlot(x, y6, "Energie-Fluss [kJ per s]", "axes x1y1 w l");
            }
            if(getParam<Scalar>("Problem.EnergyFlowDiagramSum") == 1.0)
            {
                gnuplot_.addDataSetToPlot(x, y7, "Energie-Fluss am Brunnen aufsummiert [*10 MJ]", "axes x1y1 w l");
            }
#endif
        }
    
        if(getParam<Scalar>("Problem.LanguageGnuplot") == 1.0)
        {
            gnuplot_.setXlabel("Time [h]");
      ////////////////////////ALTERNATTIIIVEEEE//////////////////////////////////
            gnuplot_.setYlabel("Different units");
            gnuplot_.addDataSetToPlot(x, y, "Pressure in aquifer [bar]", "axes x1y1 w l");
            gnuplot_.addDataSetToPlot(x, y2, "Air mass flow [kg per s]", "axes x1y1 w l");
            gnuplot_.addDataSetToPlot(x, y3, "Injected air mass [t]", "axes x1y1 w l");
            gnuplot_.addDataSetToPlot(x, y4, "Temperature in aquifer [°C]", "axes x1y1 w l");
            gnuplot_.addDataSetToPlot(x, y5, "Pressure in well [bar]", "axes x1y1 w l");
#if !ISOTHERMAL
            if(getParam<Scalar>("Problem.EnergyFlowDiagram") == 1.0)
            {
                gnuplot_.addDataSetToPlot(x, y6, "Energy flow [kJ per s]", "axes x1y1 w l");
            }
            if(getParam<Scalar>("Problem.EnergyFlowDiagramSum") == 1.0)
            {
                gnuplot_.addDataSetToPlot(x, y7, "Energy flow at well summed up [*10 MJ]", "axes x1y1 w l");
            }
#endif
        }
      //Erstes Diagramm printen
        gnuplot_.plot("inflow_plot");
    }

private:
    Scalar Permeability;
    Scalar Porosity;
    Scalar Saturation;
    Scalar Molefrac;
    Scalar Molardensity;
    Scalar Mass;
    Scalar Volume;
    Scalar MassSCV;
    Scalar MassTotal;
    
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
    PrimaryVariables priVars(0.0);
    priVars.setState(bothPhases);

    Scalar densityW = 1000.0;
    Scalar densityNw = 60.0;
    
    
    ///////Ausgangsdruck im Aquifer (Verteilung)/////////
    priVars[pressureIdx] = getParam<Scalar>("Grid.PressureOnTop") + ((((2.0 * (getParam<std::vector<Scalar>>("Grid.Axial2")[1])) - getParam<Scalar>("Grid.DomeVerschiebungOutside")) - globalPos[2])*densityW*9.81);
    if(globalPos[2] < getParam<Scalar>("Grid.plumeDistance"))
        priVars[switchIdx] = 0.001;
    else
    {
        Scalar entryP = getParam<Scalar>("BrooksCoreyModel.CapillaryPressureStrength");//has to be as in SpatialParams
        Scalar lambda = getParam<Scalar>("BrooksCoreyModel.BrooksCoreyLambda");//has to be as in SpatialParams
        Scalar resSatW = getParam<Scalar>("Component.LiquidPhaseResidualSaturation");//has to be as in SpatialParams
        Scalar resSatN = getParam<Scalar>("Component.GasPhaseResidualSaturation");//has to be as in SpatialParams
        priVars[switchIdx] = 1.0-(std::pow(((globalPos[2]-getParam<Scalar>("Grid.plumeDistance"))*(densityW-densityNw)
        *this->spatialParams().gravity(globalPos).two_norm()+entryP),(-lambda))
                                * std::pow(entryP,lambda) * (1.0-resSatW-resSatN)+resSatW);//only works for Brooks-Corey
    }
#if !ISOTHERMAL
        priVars[temperatureIdx] = getParam<Scalar>("Problem.TemperaturGanzOben") + ((getParam<std::vector<Scalar>>("Grid.Axial2")[1]) + (getParam<std::vector<Scalar>>("Grid.Axial2")[1]) - getParam<Scalar>("Grid.DomeVerschiebungOutside") - globalPos[2]) * getParam<Scalar>("Problem.TemperaturZunahmeUnten"); //40°C oben am Dome und 34,7°C/km Anstieg nach unten (Aus OldenburgPaper)
#endif

return priVars;
     }
     
    Scalar eps_;
    std::string name_;
    Scalar massInjected_;
    Scalar mutable injectionPressure_;
    Scalar energyInjected_;
    /////////////Zählen der Anzahl der Schritte///////////////////////
    int mutable stepNumberCount_;
    int mutable stepKontrolle_;
    int mutable stepNumberCountInj_;
    int mutable stepNumberCountExt_;
    /////////////Abgleichsvariablen, um zu wissen, dass wir uns an derselben NewtonIteration befinden////////////
    bool mutable abgleichsZelleChosen_;
    Scalar mutable abgleichsZellePos0_;
    Scalar mutable abgleichsZellePos1_;
    Scalar mutable abgleichsZellePos2_;   

    /////////////Zählen der verwendeten Zellen während Neumannberechnungen////////////
    int mutable numberOfUsedCells_;
    int mutable maxNumberOfUsedCells_;
    ////////////Aufaddierte Werte innerhalb der Zellen, um Durchschnittswerte und Co zu berechnen///////////
    Scalar mutable pressure_in_aquiSum_;
    Scalar mutable massInjectedFlowSum_;
    Scalar mutable massInjectedSum_;
    Scalar mutable temperature_aquiferSum_;
    Scalar mutable pressure_wellSum_;
    Scalar mutable energy_flowSum_;
    Scalar mutable energy_injectedSum_;
    /////////////////////Variablen zur Berechnung von Durchschnittstemperatur für Effizienzberechnung//////////////////
    Scalar summedUpTemperatureInj_;
    Scalar meanTemperatureInj_;
    Scalar summedUpTemperatureExt_;
    Scalar meanTemperatureExt_;
    /////////////////////Variablen zur Berechnung des Durchschnittsdrucks für Effizienzberechnung////////////////
    Scalar summedUpPressureInj_;
    Scalar meanPressureInj_;
    Scalar summedUpPressureExt_;
    Scalar meanPressureExt_;
    /////////////////////////Wirkungsgradberechnungen///////////////////////////
    /////////Mit Exergie//////////////
    Scalar exergyLoss_;
    Scalar exergyInput_;
    Scalar efficiencyViaExergy_;
    /////////////Anzahl der Zyklen///////////
    Scalar cycleNumber_;
    Scalar summedUpEfficiencies_;
    ////////////Mit Energie///////
    Scalar energyInject_;
    Scalar energyExtract_;
    Scalar efficiencyViaEnergy_;
    Dumux::GnuplotInterface<double> gnuplot_;
    bool mutable isExtracting_;
    //////notwendig, um zwischen Änderungen zu schalten///////////////
    bool mutable isFirstChange_;
    Scalar time_;
    Scalar timeStepSize_;
};
} //end namespace

#endif
