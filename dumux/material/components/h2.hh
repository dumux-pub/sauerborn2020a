// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief A class for the H2 fluid properties
 */
#ifndef DUMUX_H2_TABULATED_HH
#define DUMUX_H2_TABULATED_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/constants.hh>

#include <cmath>
#include <iostream>

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>
#include <dumux/material/components/gas.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief A class for the H2 fluid properties
 *
 * Under reservoir conditions, H2 is typically in supercritical state. These
 * properties can be provided in tabulated form, which is necessary for this
 * component implementation. The template is passed through the fluidsystem
 * brineH2fluidsystem.hh.
 * Depending on the used tabulation, the fluidsystem can also be used for gaseous H2
 */

template <class Scalar, class H2Tables>
class H2
: public Components::Base<Scalar, H2<Scalar, H2Tables> >
, public Components::Liquid<Scalar, H2<Scalar, H2Tables> >
, public Components::Gas<Scalar, H2<Scalar, H2Tables> >
{
    static const Scalar R;

    static bool warningThrown;

public:
    /*!
     * \brief A human readable name for the H2.
     */
    static std::string name()
    { return "H2"; }

    /*!
     * \brief The mass in \f$\mathrm{[kg/mol]}\f$ of one mole of H2.
     */
    static constexpr Scalar molarMass()
    { return 2.01588e-3; /* [kg/mol] */ }

    /*!
     * \brief Returns the critical temperature \f$\mathrm{[K]}\f$ of H2
     */
    static Scalar criticalTemperature()
    { return 33.2; /* [K] */ }

    /*!
     * \brief Returns the critical pressure \f$\mathrm{[Pa]}\f$ of H2
     */
    static Scalar criticalPressure()
    { return 13.0e5; /* [Pa] */ }

    /*!
     * \brief Returns the temperature \f$\mathrm{[K]}\f$ at molecular hydrogen's triple point.
     */
    static Scalar tripleTemperature()
    { return 14.0; /* [K] */ }

    /*!
     * \brief Returns the minimal tabulated pressure \f$\mathrm{[Pa]}\f$ of the used table
     */
    static Scalar minTabulatedPressure()
    { return H2Tables::tabulatedEnthalpy.minPress(); /* [Pa] */ }

    /*!
     * \brief Returns the maximal tabulated pressure \f$\mathrm{[Pa]}\f$ of the used table
     */
    static Scalar maxTabulatedPressure()
    { return H2Tables::tabulatedEnthalpy.maxPress(); /* [Pa] */ }

    /*!
     * \brief Returns the minimal tabulated temperature \f$\mathrm{[K]}\f$ of the used table
     */
    static Scalar minTabulatedTemperature()
    { return H2Tables::tabulatedEnthalpy.minTemp(); /* [K] */ }

    /*!
     * \brief Returns the maximal tabulated temperature \f$\mathrm{[K]}\f$ of the used table
     */
    static Scalar maxTabulatedTemperature()
    { return H2Tables::tabulatedEnthalpy.maxTemp(); /* [K] */ }

    /*!
     * \brief Returns true if the gas phase is assumed to be ideal
     */
    static constexpr bool gasIsIdeal()
    { return false; }

    /*!
     * \brief The vapor pressure in \f$\mathrm{[Pa]}\f$ of pure H2
     *        at a given temperature.
     * \param T the temperature \f$\mathrm{[K]}\f$
     * See:
     *
     * R. Span and W. Wagner (1996, pp. 1509-1596) \cite span1996
     */
    static Scalar vaporPressure(Scalar T)
    {
        static const Scalar a[4] =
            { -7.0602087, 1.9391218, -1.6463597, -3.2995634 };
        static const Scalar t[4] =
            { 1.0, 1.5, 2.0, 4.0 };

        // this is on page 1524 of the reference
        Scalar exponent = 0;
        Scalar Tred = T/criticalTemperature();

        using std::pow;
        for (int i = 0; i < 4; ++i)
            exponent += a[i]*pow(1 - Tred, t[i]);
        exponent *= 1.0/Tred;

        using std::exp;
        return exp(exponent)*criticalPressure();
    }

    /*!
     * \brief Specific enthalpy of gaseous H2 \f$\mathrm{[J/kg]}\f$.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     * \param pressure the pressure \f$\mathrm{[Pa]}\f$
     */
    static Scalar gasEnthalpy(Scalar temperature,
                              Scalar pressure)
    {
        if ((temperature < criticalTemperature() || pressure < criticalPressure()) && !warningThrown)
        {
            Dune::dwarn << "Subcritical values: Be aware to use "
                        <<"Tables with sufficient resolution!"<< std::endl;
            warningThrown=true;
        }
        return
            H2Tables::tabulatedEnthalpy.at(temperature, pressure);
    }

    /*!
     * \brief Specific enthalpy of liquid H2 \f$\mathrm{[J/kg]}\f$.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     * \param pressure the pressure \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidEnthalpy(Scalar temperature,
                                 Scalar pressure)
    {
        if ((temperature < criticalTemperature() || pressure < criticalPressure()) && !warningThrown)
        {
            Dune::dwarn << "Subcritical values: Be aware to use "
                        <<"Tables with sufficient resolution!"<< std::endl;
            warningThrown=true;
        }

        return gasEnthalpy(temperature, pressure);
    }

    /*!
     * \brief Specific internal energy of H2 \f$\mathrm{[J/kg]}\f$.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     * \param pressure the pressure \f$\mathrm{[Pa]}\f$
     */
    static Scalar gasInternalEnergy(Scalar temperature,
                                    Scalar pressure)
    {
        Scalar h = gasEnthalpy(temperature, pressure);
        Scalar rho = gasDensity(temperature, pressure);

        return h - (pressure / rho);
    }

    /*!
     * \brief Specific internal energy of liquid H2 \f$\mathrm{[J/kg]}\f$.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     * \param pressure the pressure \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidInternalEnergy(Scalar temperature,
                                       Scalar pressure)
    {
        Scalar h = liquidEnthalpy(temperature, pressure);
        Scalar rho = liquidDensity(temperature, pressure);

        return h - (pressure / rho);
    }

    /*!
     * \brief The density of H2 at a given pressure and temperature \f$\mathrm{[kg/m^3]}\f$.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     * \param pressure the pressure \f$\mathrm{[Pa]}\f$
     */
    static Scalar gasDensity(Scalar temperature, Scalar pressure)
    {
        if ((temperature < criticalTemperature() || pressure < criticalPressure()) && !warningThrown)
        {
            Dune::dwarn << "Subcritical values: Be aware to use "
                        <<"Tables with sufficient resolution!"<< std::endl;
            warningThrown=true;
        }
        return H2Tables::tabulatedDensity.at(temperature, pressure);
    }

    /*!
     *  \brief The molar density of H2 gas in \f$\mathrm{[mol/m^3]}\f$ at a given pressure and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     *
     */
    static Scalar gasMolarDensity(Scalar temperature, Scalar pressure)
    { return gasDensity(temperature, pressure)/molarMass(); }

    /*!
     * \brief The density of pure H2 at a given pressure and temperature \f$\mathrm{[kg/m^3]}\f$.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     * \param pressure the pressure \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidDensity(Scalar temperature, Scalar pressure)
    {
        if ((temperature < criticalTemperature() || pressure < criticalPressure()) && !warningThrown)
        {
            Dune::dwarn << "Subcritical values: Be aware to use "
                        <<"Tables with sufficient resolution!"<< std::endl;
            warningThrown=true;
        }
        return H2Tables::tabulatedDensity.at(temperature, pressure);
    }

    /*!
     * \brief The molar density of H2 in \f$\mathrm{[mol/m^3]}\f$ at a given pressure and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     *
     */
    static Scalar liquidMolarDensity(Scalar temperature, Scalar pressure)
    { return liquidDensity(temperature, pressure)/molarMass(); }

    /*!
     * \brief The pressure of steam in \f$\mathrm{[Pa]}\f$ at a given density and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param density density of component in \f$\mathrm{[kg/m^3]}\f$
     */
    static Scalar gasPressure(Scalar temperature, Scalar density)
    {
        DUNE_THROW(NumericalProblem, "H2::gasPressure()");
    }

    /*!
     * \brief The pressure of liquid water in \f$\mathrm{[Pa]}\f$ at a given density and
     *        temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param density density of component in \f$\mathrm{[kg/m^3]}\f$
     */
    static Scalar liquidPressure(Scalar temperature, Scalar density)
    {
        DUNE_THROW(NumericalProblem, "H2::liquidPressure()");
    }

    /*!
     * \brief Specific isobaric heat capacity of the component \f$\mathrm{[J/(kg*K)]}\f$ as a liquid.
     * USE WITH CAUTION! Exploits enthalpy function with artificial increment
     * of the temperature!
     * Equation with which the specific heat capacity is calculated : \f$ c_p = \frac{dh}{dT}\f$
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidHeatCapacity(Scalar temperature, Scalar pressure)
    {
        //temperature difference :
        Scalar dT = 1.; // 1K temperature increment
        Scalar temperature2 = temperature+dT;

        // enthalpy difference
        Scalar hold = liquidEnthalpy(temperature, pressure);
        Scalar hnew = liquidEnthalpy(temperature2, pressure);
        Scalar dh = hold-hnew;

        //specific heat capacity
        return dh/dT ;
    }


    /*!
     * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of H2.
     * Equations given in: - Vesovic et al., 1990
     *                     - Fenhour et al., 1998
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar gasViscosity(Scalar temperature, Scalar pressure)
    {
        bool idealViscosity = getParam<bool>("Problem.idealViscosity");

        if(idealViscosity)
        {
             /*!
            * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of \f$H_2\f$ at a given pressure and temperature.
            * \param temperature temperature of component in \f$\mathrm{[K]}\f$
            * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
            * See: R. Reid, et al.: The Properties of Gases and Liquids,
            * 4th edition (1987, pp 396-397, 667) \cite reid1987 <BR>
            * 5th edition (2001, pp 9.7-9.8 (omega and V_c taken from p. A.19)) \cite poling2001
            */
            const Scalar Tc = criticalTemperature();
            const Scalar Vc = 65.0; // critical specific volume [cm^3/mol]
            const Scalar omega = -0.216; // accentric factor
            const Scalar M = molarMass() * 1e3; // molar mas [g/mol]
            const Scalar dipole = 0.0; // dipole moment [debye]

            using std::sqrt;
            Scalar mu_r4 = 131.3 * dipole / sqrt(Vc * Tc);
            mu_r4 *= mu_r4;
            mu_r4 *= mu_r4;

            using std::pow;
            using std::exp;
            Scalar Fc = 1 - 0.2756*omega + 0.059035*mu_r4;
            Scalar Tstar = 1.2593 * temperature/Tc;
            Scalar Omega_v =
            1.16145*pow(Tstar, -0.14874) +
            0.52487*exp(- 0.77320*Tstar) +
            2.16178*exp(- 2.43787*Tstar);
            Scalar mu = 40.785*Fc*sqrt(M*temperature)/(pow(Vc, 2./3)*Omega_v);

            // convertion from micro poise to Pa s
            return mu/1e6 / 10;
        }
        else
        {
            return H2Tables::tabulatedViscosity.at(temperature, pressure);

        }
    }

    /*!
     * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of pure H2.
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidViscosity(Scalar temperature, Scalar pressure)
    {
        // no difference for supercritical H2
        return gasViscosity(temperature, pressure);
    }

    /*!
     * \brief Thermal conductivity \f$\mathrm{[[W/(m*K)]}\f$ of H2.
     *
     * Thermal conductivity of H2 at T=20°C, see:
     * http://www.engineeringtoolbox.com/carbon-dioxide-d_1000.html
     *
     * \param temperature absolute temperature in \f$\mathrm{[K]}\f$
     * \param pressure of the phase in \f$\mathrm{[Pa]}\f$
     */

    static Scalar gasThermalConductivity(Scalar temperature, Scalar pressure)
    {
        return H2Tables::tabulatedThermalConductivity.at(temperature, pressure);
    }
    
    static Scalar gasDiffusionCoefficient(Scalar temperature, Scalar pressure)
    {
        return H2Tables::tabulatedDiffusionCoefficient.at(temperature, pressure);
    }
    
        static Scalar gasComposition(Scalar temperature, Scalar pressure)
    {
        return H2Tables::tabulatedGasComposition.at(temperature, pressure);
    }
            static Scalar liquidComposition(Scalar temperature, Scalar pressure)
    {
        return H2Tables::tabulatedLiquidComposition.at(temperature, pressure);
    }
};

template <class Scalar, class H2Tables>
const Scalar H2<Scalar, H2Tables>::R = Constants<Scalar>::R;

template <class Scalar, class H2Tables>
bool H2<Scalar, H2Tables>::warningThrown = false;

} // end namespace Components

} // end namespace Dumux

#endif
