// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup InputOutput
 * \brief Interface for plotting the two-phase fluid-matrix-interaction laws
 */
#ifndef DUMUX_PLOT_DENSITY_LAW_HH
#define DUMUX_PLOT_DENSITY_LAW_HH

#include <cmath>
#include <vector>
#include <string>
#include <dumux/material/fluidstates/compositional.hh>
#include <dumux/material/components/h2.hh>

namespace Dumux {

// forward declaration
template<class Scalar> class GnuplotInterface;

/*!
 * \ingroup InputOutput
 * \brief Interface for plotting the viscosity dependent on parameters
 */
template<class Scalar, class FluidSystem,  class H2Table>
class PlotDensityLaw
{
     using FluidState = CompositionalFluidState<Scalar, FluidSystem>;
     using H2 = Dumux::Components::H2<Scalar, H2Table>;

public:
    //! Constructor
    PlotDensityLaw()
    : numIntervals_(100)
    { }

    /*!
     * \brief Add a viscosity over temperature plot
     *
     * \param gnuplot The gnuplot interface
     */
    void addDensityTemperatureDependent(GnuplotInterface<Scalar> &gnuplot,
                                          Scalar phaseIdx = 1,
                                          Scalar pressure = 1e5,
                                          Scalar xCompOne = 1,
                                          Scalar lowerTemp = 273.15,
                                          Scalar upperTemp = 308.15,
                                          std::string curveName = "rho-temp-ch4",
                                          std::string curveOptions = "w l")
    {
        std::vector<Scalar> temp(numIntervals_+1);
        std::vector<Scalar> mu(numIntervals_+1);
        Scalar tempInterval = upperTemp - lowerTemp;

        FluidState fluidState;
        fluidState.setPressure(phaseIdx, pressure);
        //fluidState.setPressure(0, 1e5);
        fluidState.setMoleFraction(phaseIdx, 0, 1-xCompOne);//this is H2O
        fluidState.setMoleFraction(phaseIdx, 1, xCompOne); //this is H2
        const Scalar T = fluidState.temperature(phaseIdx);
        const Scalar p = fluidState.pressure(phaseIdx);

        for (int i = 0; i <= numIntervals_; i++)
        {
            temp[i] = lowerTemp + tempInterval * Scalar(i) / Scalar(numIntervals_);
            fluidState.setTemperature(temp[i]);
            
            //mu[i] = FluidSystem::density(fluidState, phaseIdx); // Ausgabe Density
            //mu[i] = FluidSystem::viscosity(fluidState, phaseIdx); // Ausgabe viscosity
            //mu[i] = FluidSystem::binaryDiffusionCoefficient(fluidState, 0, 0,1); // Ausgabe diffusionCoefficient
            //mu[i] = H2::gasComposition(T,p); // Ausgabe Löslichkeit H2 in H2O
            mu[i] = FluidSystem::gascomposition(fluidState, 1, 1); // H2 in Gasphase aus PC-SAFT
            //mu[i] = FluidSystem::liquidcomposition(fluidState, 0, 1); // H2 in Flüssigphase aus PC-SAFT
            
            //Ausgabe der Zusammensetzungen:
            const Scalar p0 = fluidState.pressure(0);
            const Scalar p1 = fluidState.pressure(1);
            const Scalar partPressLiquid = FluidSystem::fugacityCoefficient(fluidState, 0, 0)*p0;

            // get the partial pressure of the main component of the gas phase
            const Scalar partPressGas = p1 - partPressLiquid;

            // calculate the mole fractions of the components within the nonwetting phase
            const Scalar xnn = partPressGas / p1;
            const Scalar xnw = partPressLiquid / p1;
                        
            const Scalar xwn = partPressGas / (FluidSystem::fugacityCoefficient(fluidState, 0, 1)*p0);
            const Scalar xww = 1.0 - xwn;
            
            //mu[i] = xwn;
            
            
            std::cout << mu[i] ;
            
            

        }

        gnuplot.setXlabel("Temperature [K]");
        gnuplot.setYlabel("Density [kg/m^3]");
        gnuplot.addDataSetToPlot(temp, mu, curveName, curveOptions);
    }

        /*!
     * \brief Add a viscosity over mole fraction plot
     *
     * \param gnuplot The gnuplot interface
     */
    void addDensityMoleFractionDependent(GnuplotInterface<Scalar> &gnuplot,
                                          Scalar phaseIdx = 1,
                                          Scalar pressure = 1e5,
                                          Scalar temp = 293.15,
                                          Scalar lowerxCompOne = 0.0,
                                          Scalar upperxCompOne = 1.0,
                                          std::string curveName = "rho-ch4",
                                          std::string curveOptions = "w l")
    {
        std::vector<Scalar> x(numIntervals_+1);
        std::vector<Scalar> mu(numIntervals_+1);

        Scalar xInterval = upperxCompOne - lowerxCompOne;

        FluidState fluidState;
        fluidState.setPressure(phaseIdx, pressure);
        fluidState.setTemperature(temp);

        for (int i = 0; i <= numIntervals_; i++)
        {
            x[i] = lowerxCompOne + xInterval * Scalar(i) / Scalar(numIntervals_);
            fluidState.setMoleFraction(phaseIdx, 1, 1-x[i]);//this is n2
            fluidState.setMoleFraction(phaseIdx, 4, x[i]);
            mu[i] = FluidSystem::density(fluidState, phaseIdx);
        }

        gnuplot.setXlabel("Mole fraction [-]");
        gnuplot.setYlabel("density");
        gnuplot.addDataSetToPlot(x, mu, curveName, curveOptions);
    }

private:

    int numIntervals_;

};//end class

} // end namespace Dumux

#endif // DUMUX_PLOT_FLUID_MATRIX_LAW_HH
