This is the DuMuX module containing the code for producing the results
submitted for:
Johannes Eller, Tim Sauerborn, Beatrix Becker, Joachim Gross, and Rainer Helmig

Modelling Subsurface Hydrogen Storage with Transport Properties from Entropy Scaling using the PC-SAFT Equation of State 
University of Stuttgart

The applications used for this publication can be found in the build directory under:
appl/injection_h2_nguyen

You can run the executable with:
./test_cc2p2cni

You can choose the termodynamic relationships and different models of transport properties in the file:
sauerborn2020a/appl/injection_h2_nguyen/test_cc2p2cni.input

The relationships from literature can be choosen by setting (for example) the 'idealViscosity' parameter to 'true'.


Preparing the Sources
=========================

Additional to the software mentioned in README you'll need the
following programs installed on your system:

  cmake >= 2.8.12

Getting started
---------------

If these preliminaries are met, you should run

  dunecontrol all

which will find all installed dune modules as well as all dune modules
(not installed) which sources reside in a subdirectory of the current
directory. Note that if dune is not installed properly you will either
have to add the directory where the dunecontrol script resides (probably
./dune-common/bin) to your path or specify the relative path of the script.

Most probably you'll have to provide additional information to dunecontrol
(e. g. compilers, configure options) and/or make options.

The most convenient way is to use options files in this case. The files
define four variables:

CMAKE_FLAGS      flags passed to cmake (during configure)

An example options file might look like this:

#use this options to configure and make if no other options are given
CMAKE_FLAGS=" \
-DCMAKE_CXX_COMPILER=g++-5 \
-DCMAKE_CXX_FLAGS='-Wall -pedantic' \
-DCMAKE_INSTALL_PREFIX=/install/path" #Force g++-5 and set compiler flags

If you save this information into example.opts you can pass the opts file to
dunecontrol via the --opts option, e. g.

  dunecontrol --opts=example.opts all

More info
---------

See

     dunecontrol --help

for further options.


The full build system is described in the dune-common/doc/buildsystem (Git version) or under share/doc/dune-common/buildsystem if you installed DUNE!


Alternatively
===============

Run the installscript following the next steps from scratch:
* Create a folder e.g. named PC-SAFT and enter it
```
mkdir PC-SAFT
cd PC-SAFT
```
* Get the installscript via
```
wget https://git.iws.uni-stuttgart.de/dumux-pub/sauerborn2020a/-/raw/master/installsauerborn2020a.sh
```
* Make it executable:
```
chmod u+x installsauerborn2020a.sh
```
* run the installscript
```
./installsauerborn2020a
```

Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir PC-SAFT
cd PC-SAFT
```

Download the container startup script

[docker_sauerborn2020a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/sauerborn2020a/-/blob/master/docker_sauerborn2020a.sh)

to that folder.

Open the Docker Container
```bash
bash docker_sauerborn2020a.sh open
```

After the script has run successfully, you may build all executables

```bash
cd sauerborn2020a/build-cmake
make build_tests
```

and you may run them individually. They are located in the build-cmake folder according to the following paths:

- appl/injection_h2_nguyen
- appl/h2h2oinjection

It can be executed with an input file, e.g.

```bash
cd appl/injection_h2_nguyen
./test_cc2p2cni test_cc2p2cni.input
```
